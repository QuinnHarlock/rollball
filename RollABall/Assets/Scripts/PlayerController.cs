﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float force;
    public Text redText;
    public Text countText;
    public Text winText;
    public Text timer;
    float myTime = 60;
    int timertext = 60;
    private Rigidbody rb;
    private int count;
    private int othercount;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        othercount = 0;
        SetCountText();
        winText.text = "";
    }

    void Update()
    {
        if (Input.GetKeyDown("space") && GetComponent<Rigidbody>().transform.position.y <= 0.5f)
        {
            Vector3 jump = new Vector3(0.0f, 200.0f, 0.0f);

            GetComponent<Rigidbody>().AddForce(jump);
     }
        timer.text = "Time left: " + myTime.ToString("f0");
        if (count < 12)
        {
            myTime -= Time.deltaTime;
        }
    }

    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

         rb.AddForce(movement * speed);
     

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        if (other.gameObject.CompareTag("Red"))
        {
            other.gameObject.SetActive(false);
            othercount = othercount + 1;
            SetCountText();
        }

    }
    void SetCountText()
    {
        redText.text = "Red Score: " + othercount.ToString();
        countText.text = "Yellow Score: " + count.ToString();
        if (count >= 12)
        {
            if (othercount >= 5)
            {
                winText.text = " You WIN !!!";
            }
        }
    }
}
