﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{

    public int speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKey(KeyCode.W))
        {
            Vector3 temp_pos = gameObject.transform.position;

            temp_pos.x += speed * Time.deltaTime;

            gameObject.transform.position = temp_pos;
        }

    }
}
